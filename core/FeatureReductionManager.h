/**
  @file FeatureReductionManager.h
  This files defines the manager class for reduction of facial feature vectors.
*/

/**
* @defgroup featureReduction
* The feature reduction algorithms pick the prominent components of the feature vectors. Thus, they reduce the size of the vector.
*/