#include "ImageFileManager.h"
#include <sstream>
#include <set>

kifas::ImageFileManager::ImageFileManager()
{
	mImageFileNumber = 0;
	mImageFileDir = "";
	mFilePathList ="";

	mColorder = 1;

    mClassNum = 0;
}

/**
* Creates the file manager with the imageDir.
* @param imageDir is the image directory to read the images from.
* @param fileListName is the path to the file that contains the list of images
*/
kifas::ImageFileManager::ImageFileManager(std::string imageDir, std::string fileListName)
{
	mImageFileNumber = 0;
	mImageFileDir = "";
	mFilePathList ="";
  setImageFileDir(imageDir);
  readFileNames(fileListName);
}

kifas::ImageFileManager::~ImageFileManager()
{
}

/**
* Read the file names and truth values given in the fileListName file. 
* This function populates the member variables mImageFilePaths and mImageFileTruthTable.
* At the same time, it counts the number of images, and store it on mImagefileNumber.
* @param fileListName is the path to the file that contains the list of files and their truth values.
*/
void kifas::ImageFileManager::readFileNames(std::string fileListName)
{
    // for calculating the number of classes
    std::set<int> classSet;

	mImageFilePaths.clear();
	mImageFileTruthTable.clear();
	mImageFilePersonID.clear();

	std::ifstream file;	
	file.exceptions ( std::ifstream::failbit | std::ifstream::badbit |std::ifstream::eofbit);
	std::string strFileName;
	char fileName[256];
	int truth_id;
	
	try
	{
        // file open
		file.open(fileListName.c_str(), std::ios_base::in);
	}
	catch(std::ifstream::failure e)
	{
    std::stringstream msg;
    msg << "Opening \"" << fileListName << "\" created an exception!";
    //TraceMessage::addError( msg.str() );
    //TraceMessage::addError( e.what() );
		throw KifasExpeption("[ImageFileManager::readFileNames] :Opening \""+fileListName+"\" created an exception! "+e.what());				
	}

	try
	{
        /// filename classID
		if(mColorder == 1)
		{
			while(!file.eof())
			{
				file >> fileName;
				file >> truth_id;

				strFileName = mImageFileDir;
				strFileName.append(fileName);

				// add list
				mImageFilePaths.push_back(strFileName);
				mImageFileTruthTable.push_back(truth_id);

                // add class id to set
                classSet.insert(truth_id);

				mImageFileNumber++;
			}
		} /// filename personID classID
		else if(mColorder == 0)
		{
			int person_id;

			while(!file.eof())
			{
				file >> fileName;
				file >> person_id;
				file >> truth_id;

				strFileName = mImageFileDir;
				strFileName.append(fileName);

				// add list
				mImageFilePaths.push_back(strFileName);
				mImageFileTruthTable.push_back(truth_id);
				mImageFilePersonID.push_back(person_id);

                // add class id to set
                classSet.insert(truth_id);

				mImageFileNumber++;
			}
		}
	}
	catch(std::ifstream::failure e)
	{
		if(file.eof() && file.fail() )
		{
			file.close();
			mIterator = mImageFilePaths.begin();
			
			TraceMessage::addMessage("The number of all images is ");
			TraceMessage::addMessageLine(mImageFileNumber);

            mClassNum = (int)(classSet.size());
            TraceMessage::addMessage("The number of classes is ");
            TraceMessage::addMessageLine(mClassNum);

			return;
		}
    std::stringstream msg;
		//msg << "When this program read \"" << fileListName << "\", a fail exception occurred before end of file!!";
		//TraceMessage::addError( msg.str() );
		throw KifasExpeption("[ImageFileManager::readFileNames] :When this program read \""+fileListName+"\", a fail exception occurred before end of file!! "+e.what());
	}

	file.close();
}

bool kifas::ImageFileManager::resetFileIndex()
{	
	mIterator = mImageFilePaths.begin();
	return true;
}

/**
* Sets the image directory, and checks for the ending slash (this function adds the slash if it is not present).
* @param imageDir is the image directory to read the images from.
*/
void kifas::ImageFileManager::setImageFileDir(std::string imageDir)
{
	mImageFileDir = imageDir;

	char checkstr = mImageFileDir.at(mImageFileDir.size()-1);

	// Check whether the directory ends with a slash, and add it if it's not the case
	if( checkstr != '/' || checkstr != '\\')
		mImageFileDir.append("/");   // Linux , Window are different
}

std::string kifas::ImageFileManager::getNextImageFilePath()		// ���� ���� �н� �������� �Լ�
{
	if(mIterator != mImageFilePaths.end())
	{
		std::list<std::string>::iterator tempIterator = mIterator;	
		mIterator++;
		return *tempIterator;
	}
	else
	{
		return "0";
	}

}
