#include "NEDP_17.h"
#include "ConfigFileManager.h"
#include "Parser.h"
#include <stdlib.h>     /* abs */
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#define PI 3.14159265
using namespace cv;

NEDP_17::NEDP_17() 
{
	mConfigFile = "NEDP_17.conf";
	std::map<std::string, kifas::NumStr> nedpconfig;
	kifas::ConfigFileManager::ConfigFileRead(mConfigFile, nedpconfig);
		
	edgepercent = nedpconfig["edgepercent"];
	scorethresh = nedpconfig["scorethresh"];	
	sigma = nedpconfig["sigmaw"];	
	ignorecode = nedpconfig["ignorecode"];
	padding = nedpconfig["padding"];
}


cv::Mat NEDP_17::runCodeGenerate(const cv::Mat input)
{
	
	cv::Mat sourceImage, MAGNITUDE, PHASE;
	cv::Mat dstCode, score;

	if (input.channels() >= 3)
		cv::cvtColor(input, sourceImage, CV_RGB2GRAY);
	else
		sourceImage = input;
	
	/// Generate grad_x and grad_y
	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;

	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;

	/// Gradient X
	Sobel(sourceImage, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_x, abs_grad_x);

	/// Gradient Y
	Sobel(sourceImage, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_y, abs_grad_y);

	MAGNITUDE = cv::Mat::zeros(grad_x.rows, grad_x.cols, CV_8U);
	int mTmp;
	for (int i = 0; i < grad_x.rows; i++)
	{
		for (int j = 0; j < grad_x.cols; j++)
		{
			mTmp = hypot(abs_grad_x.at<uchar>(i, j), abs_grad_y.at<uchar>(i, j));
			if (mTmp > 255.f) mTmp = 255;
			MAGNITUDE.at<uchar>(i, j) = mTmp;
		}
	}

	////to store the gradients
	PHASE = Mat::zeros(MAGNITUDE.rows, MAGNITUDE.cols, CV_32F);
	grad_x.convertTo(grad_x, CV_32F);
	grad_y.convertTo(grad_y, CV_32F);

	phase(grad_x, grad_y, PHASE, true);

	for (int i = 0; i < grad_x.rows; i++)
	{
		for (int j = 0; j < grad_x.cols; j++)
		{
			if (PHASE.at<float>(i, j) != 0.0f)
				PHASE.at<float>(i, j) = 360.0f - PHASE.at<float>(i, j);
		}
	}
	magThresh = ComputeMagThreshold(MAGNITUDE, edgepercent);

	
	
	//general variables
	int i = 0, j = 0, k = 0;
	int temp;
	
	//Gaussian Weight definition
	double *ANGLEIDEAL, *SimAGNLE, *SimMag;	
	ANGLEIDEAL = gaussianTable(90);
	
	int xIndx[8] = { 1, 1,  0,   -1, -1, -1,  0, 1 };
	int yIndx[8] = { 0, -1, -1,  -1,  0,  1,  1, 1 };

	int nAng, nMag, cMag, cAng;
	struct { float val; int indx; } max1, max2, max3, tmp, firstS;
	float tVal, supp_tval = 0, mgSWi = 0;

	int indxC, indxL, indxR, gdCP, gdNP, cpX, cpY;
	float  mgCP, mgNP, tmpScore;
	int dif, gd1, gd2;
	unsigned char tcode, sign1, sign2;
	

	//create output Image
	dstCode = Mat::zeros(MAGNITUDE.size().height - padding *2, MAGNITUDE.size().width - padding *2, MAGNITUDE.type());
	int dx = dstCode.size().width;
	int dy = dstCode.size().height;


	int p, q;
	for (  p = 0;  p < dy ; p++)
		for (q = 0; q < dx ; q++)
		{
					
			i = p + padding; j = q + padding;

			
 			if ((cMag = MAGNITUDE.at<uchar>(cvPoint(j, i)))>magThresh)  			{
				cAng = PHASE.at<float>(cvPoint(j, i));             

				max1.val = max2.val = max3.val = 1.0; 
				for (k = 0; k<8; k++)
				{

					if ((nMag = MAGNITUDE.at<uchar>(cvPoint(j + xIndx[k], i + yIndx[k])))>magThresh) 
					{

						nAng = PHASE.at<float>(cvPoint(j + xIndx[k], i + yIndx[k]));   
						
						int ID_angDiff = angleDiff(k, nAng);    float idANgleD = ANGLEIDEAL[ID_angDiff];
											
					
						tVal = nMag *  idANgleD;     
						

						 // update max
						if (tVal>scorethresh && tVal>max3.val) 
						{
							max3.val = tVal;  max3.indx = k;
							if (max3.val>max2.val)
							{
								tmp = max2; max2 = max3; max3 = tmp;
								if (max2.val>max1.val) { tmp = max1; max1 = max2; max2 = tmp; }
							}
						}

					}
				}


						
				// assign code
				if (max1.val == 1) { dstCode.at<uchar>(cvPoint(q, p)) = ignorecode; }	

				else
				{

					if (max1.val>1 && max2.val>1) 					{
						gd1 = PHASE.at<float>(cvPoint(j + xIndx[max1.indx], i + yIndx[max1.indx]));
						gd2 = PHASE.at<float>(cvPoint(j + xIndx[max2.indx], i + yIndx[max2.indx]));


						sign1 = calcSIGN ( max1.indx, gd1 );
						sign2 = calcSIGN ( max2.indx, gd2);

						

						if (max1.indx < max2.indx)
							dstCode.at<uchar>(cvPoint(q,p)) = (max1.indx + sign1) * 8 + max2.indx;
						else
							dstCode.at<uchar>(cvPoint(q,p)) = max2.indx * 16 + (sign1 + max1.indx);
						
					} 

					else if (max1.val>1 && max2.val == 1) // ENDOFLINE
					{
						
						gd1 = PHASE.at<float>(cvPoint(j + xIndx[max1.indx], i + yIndx[max1.indx]));						
						sign1 = calcSIGN ( max1.indx, gd1 );
						
						dstCode.at<uchar>(cvPoint(q,p)) = (max1.indx + sign1) * 8 + max1.indx;
					} 


					else { dstCode.at<uchar>(cvPoint(q,p)) = ignorecode; }  
				}
			}
		else { dstCode.at<uchar>(cvPoint(q,p)) = ignorecode;  }
		}

	delete ANGLEIDEAL, SimAGNLE, SimMag;
	return dstCode;
}



int NEDP_17::getCodeSize()
{
	return 128; 
}



double* NEDP_17::gaussianTable(int radius) {


	int kernalSize = 2 * radius + 1;
	double c = 2.0 * sigma * sigma;

	double* kernel = NULL;
	kernel = new double[kernalSize];
	double x;

	for (int i = 0; i < kernalSize; ++i) {
		x = i - kernalSize / 2;
		kernel[i] = exp(-x * x / c) / sqrt(c * 3.1416);
	}


	//Generating weight
	double* weightTable = NULL; 	weightTable = new double[radius + 1];
	int w = 1; weightTable[0] = 1.0;
	double ScaleFactor = 0.95 / kernel[radius];

	for (int kp = radius; kp >= 1; kp--) {
		weightTable[w] = kernel[kp] * ScaleFactor;
		//cout << "\n" << w << "= " << weightTable[w];
		w++;
	}

	delete[] kernel;
	kernel = NULL;
	return weightTable;
}


//Calcualte difference
int NEDP_17::angleDiff( int position, float angle) {
	
	float diff1 = 0, diff2=0;

	//0,4
	if (position == 0 || position == 4) {

		diff1 = abs(90 - angle);
		diff2 = abs(270 - angle);
	}


	//2,6
	else if (position == 2 || position == 6) {

		if (angle>270 && angle <= 360)   diff1 = abs(360 - angle);
		else diff1 = abs(0 - angle);

		diff2 = abs(180 - angle);
	}


	//5,1
	else if (position == 5 || position == 1) {

		if (angle >= 0 && angle <45)   diff1 = angle + 45;
		else diff1 = abs(315 - angle);

		diff2 = abs(135 - angle);
	}

	//3,7
	else if (position == 3 || position == 7) {

		if (angle >= 315 && angle <360)   diff1 = (360 - angle) + 45;
		else diff1 = abs(45 - angle);

		diff2 = abs(225 - angle);
	}

	//return 
	int DIF;
	if (diff1 <= diff2) 	DIF = diff1;
	else DIF = diff2;

	return DIF;
}



int NEDP_17::calcSIGN (int position, float angle) {
	
	int sign = 0;
	
	
	//0,4
	if (position == 0 || position == 4) {

		if (angle >= 0 && angle <= 180)
			sign = 0;
		else
			sign = 8;
	}

	//2,6
	else if (position == 2 || position == 6) {
		
		if (angle >= 90 && angle <= 270)
			sign = 8;
		else 
			sign = 0;
    	}

	//1,5
	else if (position == 1 || position == 5) {

		if (angle >= 45 && angle <= 225)
			sign = 0;
		else
			sign = 8;
	}

	//3,7
	else if (position == 3 || position == 7) {

		if (angle >= 135 && angle <= 315)
			sign = 8;
		else
			sign = 0;
	}
	

	return sign;
}


int NEDP_17::ComputeMagThreshold(cv::Mat gradMag, float mT1Percent)
{
	int height = gradMag.size().height;
	int width = gradMag.size().width;
	//int mWindowSize = padding;
	int totalPixels = (height - (padding * 2)) * (width - (padding * 2));
	int mag_bin = 0, tmp;
	float sum = 0;


	//// Generate histogram of magnitudes, to see the distribution
	int hist_mag[256];
	std::fill(hist_mag, hist_mag + 256, 0);


	for (int i = padding; i < (height - padding); i++)
	{
		for (int j = padding; j < (width - padding); j++)
		{
			tmp = gradMag.at<uchar>(i, j);
			hist_mag[tmp]++;
		}
	}


	for (int h = (256 - 1); h >= 0; h--)
	{
		sum = sum + hist_mag[h];
		if ((sum / totalPixels) > mT1Percent)
		{
			mag_bin = h;
			break;
		}
	}

	return mag_bin;
}






